$(document).ready(function() {
    $('html').on('click', function() {
        $('.phone-block').removeClass('active')
    })

    $('.phone-block').on('click', function(e) {
        e.stopPropagation()
    })

    $('.phone-block__toggle').on('click', function() {
        $(this).parent().toggleClass('active');
    })
    /*
    // проверка на наличие подменю, чтобы проставить стрелки
    $('.menu > li').each(function() {
        if($(this).children('.submenu').length > 0) {
            $(this).addClass('has-submenu')
        } else {
            $(this).removeClass('has-submenu')
        }
    })
    */
    // slider on the first sreen
    $('.first-screen__slider').slick({
        slidesToShow: 3,
        arrows: false,
        dots: true,
        swipeToSlide: true,
        autoplay: true,
        responsive: [
            {
              breakpoint: 769,
              settings: {
                slidesToShow: 2,
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
              }
            }
          ]
    })

     // news slider
     $('.news-block-slider').each(function () {
      $(this).slick({
        slidesToShow: 3,
        //swipeToSlide: true,
        prevArrow: '<span class="prev-arrow"><svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.37634 1L0.876343 7.5L7.37634 14" stroke="black"/></svg></span>',
        nextArrow: '<span class="next-arrow"><svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.623657 1L7.12366 7.5L0.623657 14" stroke="black"/></svg></span>',
        appendArrows: $(this).closest('.tab_content').find('.news-block-slider-arrows'),
        responsive: [
            {
              breakpoint: 769,
              settings: {
                slidesToShow: 2,
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
              }
            }
          ]
      })
    });

    // practic slider
    $('.practic-slider').slick({
        slidesToShow: 1,
        arrows: false,
        dots: true,
        autoplay: true,
        slidesToScroll: 1,
    })

    // partner slider
    $('.partners-slider').slick({
        slidesToShow: 6,
        arrows: true,
        dots: false,
        autoplay: true,
        slidesToScroll: 1,
        swipeToSlide: true,
        prevArrow: '<span class="prev-arrow"><svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.37634 1L0.876343 7.5L7.37634 14" stroke="black"/></svg></span>',
        nextArrow: '<span class="next-arrow"><svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.623657 1L7.12366 7.5L0.623657 14" stroke="black"/></svg></span>',
        appendArrows: '.partners-slider-arrows',
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2,
              }
            }
          ]
    })

    // raiting slider
    $('.raiting-slider').slick({
        slidesToShow: 1,
        arrows: false,
        dots: true,
        autoplay: false,
        slidesToScroll: 1,
        appendDots:$('.raiting-slider-nav')
    })

    // testimonial slider
    $('.testimonials-slider').slick({
        slidesToShow: 1,
        arrows: false,
        dots: true,
        autoplay: false,
        slidesToScroll: 1,
        appendDots:$('.testimonials-slider-nav')
    })

    // contact slider
    const contactSliderCount = 3
    if($('.contact-slider .slide').length > contactSliderCount) {
      $('.contact-slider').slick({
        slidesToShow: contactSliderCount,
        arrows: true,
        dots: false,
        autoplay: false,
        slidesToScroll: 1,
        swipeToSlide: true,
        prevArrow: '<span class="prev-arrow"><svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.37634 1L0.876343 7.5L7.37634 14" stroke="black"/></svg></span>',
        nextArrow: '<span class="next-arrow"><svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.623657 1L7.12366 7.5L0.623657 14" stroke="black"/></svg></span>',
        appendArrows: '.contact-slider-arrows',
        responsive: [
            {
              breakpoint: 769,
              settings: {
                slidesToShow: 2,
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
              }
            }
          ]
    })
    }

    // association slider
    $('.association-slider').slick({
      infinite:false,
      slidesToShow: 6,
      arrows: true,
      dots: false,
      autoplay: false,
      slidesToScroll: 1,
      swipeToSlide: true,
      prevArrow: '<span class="prev-arrow"><svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.37634 1L0.876343 7.5L7.37634 14" stroke="black"/></svg></span>',
      nextArrow: '<span class="next-arrow"><svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.623657 1L7.12366 7.5L0.623657 14" stroke="black"/></svg></span>',
      appendArrows: '.association-slider-arrows',
      responsive: [
        {
          breakpoint: 769,
          settings: {
            slidesToShow: 4,
            variableWidth:false,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 3,
            variableWidth:false,
          }
        }
      ]
    })

    // events slider
    $('.events-slider').slick({
      infinite:false,
      slidesToShow: 2,
      arrows: true,
      dots: false,
      autoplay: false,
      slidesToScroll: 1,
      swipeToSlide: true,
      prevArrow: '<span class="prev-arrow"><svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.37634 1L0.876343 7.5L7.37634 14" stroke="black"/></svg></span>',
      nextArrow: '<span class="next-arrow"><svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.623657 1L7.12366 7.5L0.623657 14" stroke="black"/></svg></span>',
      appendArrows: '.events-slider-arrows',
      variableWidth:true,
      responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              variableWidth:false,
            }
          },
          {
            breakpoint: 769,
            settings: {
              slidesToShow: 2,
              variableWidth:false,
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              variableWidth:false,
            }
          }
        ]
     })

     // recomendation slider
    $('.recomendation-slider').slick({
      infinite:false,
      slidesToShow: 4,
      arrows: true,
      dots: false,
      autoplay: false,
      slidesToScroll: 1,
      swipeToSlide: true,
      prevArrow: '<span class="prev-arrow"><svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.37634 1L0.876343 7.5L7.37634 14" stroke="black"/></svg></span>',
      nextArrow: '<span class="next-arrow"><svg width="8" height="15" viewBox="0 0 8 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.623657 1L7.12366 7.5L0.623657 14" stroke="black"/></svg></span>',
      appendArrows: '.recomendation-slider-arrows',
      responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 2,
            }
          }
        ]
     })


    // Popup для поиска
    // $('.js--popup').on('click', function (e) {
    // e.preventDefault();
    //   let btn = $(this).data('modal');
    //   $('#' + btn).addClass('active');
    //   })
  
    //   $('.close-search').on('click', function (e) {
    //   e.preventDefault();
    //   $('.search-window').removeClass('active');
    // })

    // Popup для блока истории
    $('.js--popup').on('click', function (e) {
      e.preventDefault();
      let btn = $(this).data('modal');
      $('#' + btn).addClass('active');
      $('.popup-overlay').show();
      $('body').toggleClass('noscroll');
    })

    $('.close-popup, .popup-overlay, .close-search, .close').on('click', function (e) {
      e.preventDefault();
      $('.search-window').removeClass('active');
      $('.popup-overlay').hide();
      $('.popup-window').removeClass('active');
      $('body').removeClass('noscroll');
    })


    // mobile menu
    $('.mobile-btn').on('click', function () {
      $('body').addClass('noscroll');
      $('.nav').addClass('active');
    });
    $('.close-menu').on('click', function () {
      $('body').removeClass('noscroll');
      $('.nav').removeClass('active');
    });
    // Подменю
    $('.have-sub').click(function () {
        $(this).closest('li').toggleClass('active');
        $(this).toggleClass('active').next().slideToggle();
        $('.have-sub').not(this).removeClass('active').next().slideUp();
        $('.have-sub').not(this).closest('li').removeClass('active');
    });
  
    // tabs alt
    $(".js--tab").click(function () {
      $(".tab_content").removeClass("active");
      var activeTab = $(this).attr("rel");
      $("#" + activeTab).addClass("active");
      $(".js--tab").removeClass("active");
      $(this).addClass("active");
  });

    // Выпадающий список
    $('.select').on('click','.placeholder',function(){
      var parent = $(this).closest('.select');
      if ( ! parent.hasClass('is-open')){
      parent.addClass('is-open');
      $('.select.is-open').not(parent).removeClass('is-open');
      }else{
      parent.removeClass('is-open');
      }
    }).on('click','ul>li',function(){
      var parent = $(this).closest('.select');
      parent.removeClass('is-open').find('.placeholder').text( $(this).text() );
      parent.find('input[type=hidden]').attr('value', $(this).attr('data-value') );
    });
    // -- Закрываем селект при клике вне элемента и при скролинге
    $(document).click( function(e){
      if ( $(e.target).closest('.select').length ) {
        return;
      }
      $('.select').removeClass('is-open');
    });
    $(document).scroll(function (e){  
      if ( $(e.target).closest('.select').length ) {
        return;
      }
      $('.select').removeClass('is-open');
    });

    // Показываем скрытые блоки
    $('.show-more').click(function (e) {
      e.preventDefault();
      $(this).closest('body').toggleClass('show-hidden-blocks');
    });

    // раскрытие текста
    $('.show-more').on('click', function() {
      $(this).closest('.history-info').toggleClass('show-text')

      if($(this).closest('.history-info').hasClass('show-text')) {
        $(this).text('Свернуть ')
      } else {
        $(this).text('Подробнее ')
      }
    })

})